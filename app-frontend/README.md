# Start App
Clone repo, install, cd into folder and run:
```git
npm install
npm start
```
# Description 

Unauthorized user : Cannot add comments & Cannot edit profile 
Authorized user : Can add comments & Can edit own profile 

Database relationships : User -> Comment ( one-to-many )
						 User -> Profile ( one-to-one )

Main ideas : 

Unauthorized user register -> one user & one profile are created .

For recognizing the user id when adding comments or editing the profile I got the id of the user by the email given when logged or registered .

For authorized routes I created sessions with user's token and when the user log out then the session is removed .

If an user is logged then requiring to login or register are useless so the user is redirected to home page . A registered user can only log out .
A guest can login and register . A log out for an unauthorized user is useless so it is redirected to login page . 