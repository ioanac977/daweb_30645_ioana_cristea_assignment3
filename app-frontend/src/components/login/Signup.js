import React, {Component} from 'react';
import {PostData} from '../../service/PostData';
import {Redirect} from 'react-router-dom';
import '../../styles/Signup.css';
import ApiServiceProfile from "../../service/ApiServiceProfile";
import ApiServiceUser from "../../service/ApiServiceUser";

class Signup extends Component {

  constructor(props){
    super(props);

    this.state = {
     // username: '',
        name: '',
        email: '',
        user_id:'',
     password: '',


     redirectToReferrer: false
    };

    this.signup = this.signup.bind(this);
    this.onChange = this.onChange.bind(this);

  }

  signup() {
    if( this.state.password && this.state.email && this.state.name){
    PostData('/register',this.state).then((result) => {
      let responseJson = result;
      if(responseJson.user){
          window.localStorage.setItem("emailUtilizatorLogat",this.state.email);
          ApiServiceUser.getUserIdByEmail(this.state.email)
              .then((res) => {
                  let user = res.data[0];
                  this.setState({
                      user_id: user.id,

                  })
                  let profile = {
                      name: this.state.name,
                      email: this.state.email,
                      password: this.state.password,
                      interests: "unknown",
                      user_id: user.id
                  }
                  ApiServiceProfile.createUserProfile(profile)
                      .then(res => {
                          this.setState({message : 'Profile for user created successfully.'});
                      });
              });


        sessionStorage.setItem('userData',JSON.stringify(responseJson));
        this.setState({redirectToReferrer: true});
      }

     });
    }
  }

 onChange(e){
   this.setState({[e.target.name]:e.target.value});
  }

  render() {
    if (this.state.redirectToReferrer || sessionStorage.getItem('userData')) {
      return (<Redirect to={'/'}/>)
    }



    return (

        <div className="registerBox">

            <img
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZZKMV-vK-VG3SR7R9ku-_l3JU2wC-u6PRzS4-_WluxAatvobb"
                className="user">

            </img>
            <h2>Register</h2>

            <p>Name</p>
            <input type="text" name="name"  placeholder="Name" onChange={this.onChange}/>
            <p>Email</p>
            <input type="text" name="email"  placeholder="Email" onChange={this.onChange}/>
            <p>Password</p>
            <input type="password" name="password"  placeholder="Password" onChange={this.onChange}/>
            <input type="submit" className="button" value="Sign Up" onClick={this.signup}/>
            <a href="/login">Login</a>


        </div>

    );
  }
}

export default Signup;
