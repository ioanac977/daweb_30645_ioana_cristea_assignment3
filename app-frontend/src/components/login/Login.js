import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {PostData} from '../../service/PostData';
import '../../styles/Login.css';

class Login extends Component {

    constructor(){
        super();

        this.state = {
            email:'',
            password: '',
            userDetails:{},
            redirectToReferrer: false
        };

        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);

    }

    // "email:"+this.state.email+", password:"+this.state.password

    login() {
        this.userDetails = {
            email: this.state.email,
            password: this.state.password
        }
        if(this.state.email && this.state.password){
            PostData('/login',this.userDetails).then((result) => {
                let responseJson = result;
                if(responseJson.token){
                    window.localStorage.setItem("emailUtilizatorLogat",this.state.email);
                    sessionStorage.setItem('userData',JSON.stringify(responseJson));
                    this.setState({redirectToReferrer: true});
                }

            });
        }

    }

    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }




    render() {

        if (this.state.redirectToReferrer) {
            return (<Redirect to={'/'}/>)
        }

        if(sessionStorage.getItem('userData')){
            return (<Redirect to={'/'}/>)
        }

        return (

            <div className="loginBox">

                <img
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZZKMV-vK-VG3SR7R9ku-_l3JU2wC-u6PRzS4-_WluxAatvobb"
                    className="user">

                </img>
                <h2>Login</h2>

                <p>Username</p>
                <input type="text" name="email" placeholder="Email" onChange={this.onChange}/>
                <p>Password</p>
                <input type="password" name="password"  placeholder="Password" onChange={this.onChange}/>
                <input type="submit" className="button success" value="Login" onClick={this.login}/>
                <a href="/signup">Registration</a>

            </div>


        );
    }
}

export default Login;
