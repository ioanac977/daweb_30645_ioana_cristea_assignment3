import React, {Component} from 'react';
import {CardText, Cell} from 'react-mdl';
import "../styles/despre.css"
import getTranslation from "../constants/getTranslation";
import {store} from "../constants/store"
import {TextareaAutosize} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import ApiServiceEmail from "../service/ApiServiceEmail";
import ApiServiceComments from "../service/ApiServiceComments";
import Redirect from "react-router-dom/Redirect";
import ApiServiceUser from "../service/ApiServiceUser";

class Despre extends Component {

    constructor(props){
        super(props);
        this.state ={
            description:'',
            user_id:1,
            redirectToReferrer:false,
            message: null
        }
         this.addComment = this.addComment.bind(this);
        this.loadUser = this.loadUser.bind(this);
    }

    componentDidMount() {
        this.loadUser();

    }

    loadUser() {
        ApiServiceUser.getUserIdByEmail(window.localStorage.getItem("emailUtilizatorLogat"))
            .then((res) => {
                let user = res.data[0];
                this.setState({
                   user_id: user.id,

                })
            });
    }
    componentWillMount = () => {
        this.selectedCheckboxes = new Set();
        if (sessionStorage.getItem("userData")) {
            // this.getUserFeed();
        } else {
            this.setState({redirectToReferrer: true});
        }
    }
    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    addComment = (e) => {
        e.preventDefault();

        if (this.state.redirectToReferrer) {
           this.props.history.push('/login');
        }else {
            let comment = {description: this.state.description, user_id: this.state.user_id};

            ApiServiceComments.addComment(comment)
                .then(res => {
                    this.setState({message: 'Comment added successfully.'});
                    this.props.history.push('/success');
                });
        }

    }

    render() {
        return (

            <div>
                <div className="landing-grid">


                </div>

                <div className="text-box">

                    <h1 className="heading-primary">
                        <span className="heading-primary-main">Planning Gps</span>
                        <span className="heading-primary-sub">using</span>
                        <span className="heading-primary-main">Google APIs</span>
                    </h1>


                </div>

                <div className="container">

                    <div className="sticky-notes">
                        <form action="get" id="survey-form">

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 1:</legend>
                                <fieldset>
                                    <legend>User authentication/creation:</legend>
                                    <CardText>Sign in</CardText>
                                    <CardText>Register</CardText>
                                    <CardText>CRUD Account</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 2:</legend>
                                <fieldset>
                                    <legend>Choose the interest area:</legend>
                                    <br>
                                    </br>
                                    <select name="location" id="dropdown">
                                        <optgroup label="Africa">
                                            <option value="">Algeria</option>
                                            <option value="">Botswana</option>
                                            <option value="">Madagascar</option>
                                            <option value="">Uganda</option>
                                        </optgroup>
                                        <optgroup label="Asia">
                                            <option value="">Afghanistan</option>
                                            <option value="">India</option>
                                            <option value="">Pakistan</option>
                                            <option value="">Russia</option>
                                        </optgroup>
                                        <optgroup label="Europe">
                                            <option value="">Albania</option>
                                            <option value="">Croatia</option>
                                            <option value="">Georgia</option>
                                            <option value="">Romania</option>
                                        </optgroup>
                                        <optgroup label="South America">
                                            <option value="">Argentina</option>
                                            <option value="">Brazil</option>
                                            <option value="">Chile</option>
                                            <option value="">Venezuela</option>
                                        </optgroup>
                                    </select>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 3:</legend>
                                <fieldset>
                                    <legend> Create a plan:</legend>
                                    <CardText>Select a week</CardText>
                                    <CardText> Add pins on locations</CardText>
                                    <CardText> CRUD cell activity</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 4:</legend>
                                <fieldset>
                                    <legend> Plan management:</legend>
                                    <CardText>View plans</CardText>
                                    <CardText> View activity charts</CardText>
                                    <CardText> Smart routes</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 5:</legend>
                                <fieldset>
                                    <legend>Interface and app marketing:</legend>
                                    <CardText>Choose interface theme</CardText>
                                    <CardText> Rating the application </CardText>
                                    <CardText> View latest rates</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 6:</legend>
                                <fieldset>
                                    <legend>Security:</legend>
                                    <CardText>Encrypted password</CardText>
                                    <CardText> Routes security </CardText>
                                    <CardText> Cookie session</CardText>
                                </fieldset>
                            </fieldset>

                            <fieldset className="sticky-note">
                                <legend className="tape">{getTranslation(
                                    store.getState(),
                                    'obiectiv/objectiv'
                                )} 7:</legend>
                                <fieldset>
                                    <legend>Usability:</legend>
                                    <CardText>Friendly interface</CardText>
                                    <CardText> Guest session </CardText>
                                    <CardText> Demo </CardText>
                                </fieldset>
                            </fieldset>

                        </form>
                        <label>
                            Comment :
                        </label>
                        <br></br>
                        <TextareaAutosize style={{marginLeft:"-10px",width:"550px",height:"120px",background:"rgb(217, 239, 203)"}} placeholder="description" fullWidth margin="normal" name="description" value={this.state.description} onChange={this.onChange}/>
                        <br></br>
                        <Button style={{marginLeft:"-10px",width:"550px",height:"20px",background:"#3da4ab"}} variant="contained" onClick={this.addComment}>Add Comment</Button>
                        <br></br>

                    </div>
                    <footer>
                        Made by <a href ="https://github.com/ioanac977/presentation-resume-template">@IoanaCristea</a>
                    </footer>

                </div>

            </div>


        )
    }
}

export default Despre;
