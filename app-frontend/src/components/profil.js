import React, {Component} from 'react';
import {Cell, Grid} from "react-mdl";
import profilPhoto from "../images/profilPhoto3.jpg";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Checkbox from "../constants/Checkbox";
import Redirect from "react-router-dom/Redirect";
import ReactUploadImage from "../constants/ReactUploadImage";
import axios from "axios";
import ApiServiceUser from "../service/ApiServiceUser";
import ApiServiceProfile from "../service/ApiServiceProfile";



const items = [
    ' HTML/CSS ',
    ' JavaScript ',
    ' React ',
    ' NodeJS ',
    ' Java Spring Boot ',
    ' MySQL '
];

class Profil extends Component {

    constructor(props){
        super(props);
        this.state ={
            profile_id:'',
            name:'',
            email:'',
            password:'',
            user_id:1,
            redirectToReferrer:false,
            message: null
        }
        // this.sendEmail = this.sendEmail.bind(this);
    }
    componentDidMount() {
        this.loadUser();

    }

    loadUser() {
        ApiServiceUser.getUserIdByEmail(window.localStorage.getItem("emailUtilizatorLogat"))
            .then((res) => {
                let user = res.data[0];
                this.setState({
                    user_id: user.id,

                })
                ApiServiceProfile.getProfileByUserId(user.id)
                    .then((res) => {
                        let profile = res.data[0];
                        this.setState({
                            profile_id: profile.id,

                        })

                    });
            });
    }

    componentWillMount = () => {
        this.selectedCheckboxes = new Set();
        if (sessionStorage.getItem("userData")) {
            // this.getUserFeed();
        } else {
            this.setState({redirectToReferrer: true});
        }
    }
    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    toggleCheckbox = label => {
        if (this.selectedCheckboxes.has(label)) {
            this.selectedCheckboxes.delete(label);
        } else {
            this.selectedCheckboxes.add(label);
        }
    }

    handleFormSubmit = formSubmitEvent => {
        formSubmitEvent.preventDefault();
        let interests=" ";
        for (const checkbox of this.selectedCheckboxes) {
            console.log(checkbox, 'is selected.');
            interests +=" "+checkbox;
        }
        sessionStorage.getItem("userData");
        if (this.state.redirectToReferrer) {
            this.props.history.push('/login');
        }else {
            let profile = {
                id:this.state.profile_id,
                name: this.state.name,
                email: this.state.email,
                password: this.state.password,
                interests: interests,
                user_id: this.state.user_id
            }
            ApiServiceProfile.updateUserProfile(profile)
                .then(res => {
                    this.setState({message : 'User profile updated successfully.'});
                    this.props.history.push('/success');
                });
        }

    }

    createCheckbox = label => (
        <Checkbox
            label={label}
            handleCheckboxChange={this.toggleCheckbox}
            key={label}
        />
    )

    createCheckboxes = () => (
        items.map(this.createCheckbox)
    )
    render() {

        return (
            <div style={{width: '100%', margin: 'auto'}}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <img

                            src={profilPhoto}
                            alt="profilPhoto"
                            className="avatar-img"
                        />

                        <div className="banner-text">
                            {/*<h1>Cristea Ioana </h1>*/}
                            <TextField  style={{background:"orange"}} placeholder="name" fullWidth margin=""  name="name" value={this.state.name} onChange={this.onChange}/>
                            <TextField style={{background:"orange"}} placeholder="email" fullWidth margin=""  name="email" value={this.state.email} onChange={this.onChange}/>
                            <TextField style={{background:"orange"}} placeholder="password" fullWidth margin="" name="password" value={this.state.password} onChange={this.onChange}/>
                            <hr/>

                            <ReactUploadImage/>
                            <div className="row">
                                {/*<div className="col-sm-12">*/}


                                <h5 style={{color:"white"}}> Interests :</h5>
                                    <form onSubmit={this.handleFormSubmit}>

                                        {this.createCheckboxes()}

                                        <button style={{width:"100px",height:"70px",background:"orange"}} className="btn btn-default" type="submit">Save</button>
                                    </form>

                                {/*</div>*/}
                            </div>


                            <div className="social-links">

                                {/* LinkedIn */}
                                <a href="http://google.com" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-linkedin-square" aria-hidden="true"/>
                                </a>

                                {/* Github */}
                                <a href="http://google.com" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-github-square" aria-hidden="true"/>
                                </a>

                                {/* Freecodecamp */}
                                <a href="http://google.com" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-free-code-camp" aria-hidden="true"/>
                                </a>

                                {/* Youtube */}
                                <a href="http://google.com" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-youtube-square" aria-hidden="true"/>
                                </a>

                            </div>
                        </div>
                    </Cell>
                    <footer> <br></br><br></br><br></br>
                        Made by <a href ="https://github.com/ioanac977/presentation-resume-template">@IoanaCristea</a>
                    </footer>
                </Grid>

            </div>

        )
    }
}

export default Profil;
