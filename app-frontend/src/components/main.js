import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Noutati from './noutati';
import Contact from './contact';
import Despre from './despre';
import Acasa from './acasa';
import Profil from './profil';
import Coordonator from "./coordonator";
import ConvertXMLtoREACT from "./xmlFile/convertXMLtoREACT";
import Success from "./response/success";
import Login from "./login/Login";
import Signup from "./login/Signup";
import Logout from "./login/Logout";




const Main = () => (

    <Switch>
        <Route exact path="/" component={Acasa}/>
        <Route path="/acasa" component={Acasa}/>
        <Route path="/contact" component={Contact}/>
        <Route path="/despre" component={Despre}/>
        <Route path="/noutati" component={Noutati}/>
        <Route path="/profil" component={Profil}/>
        <Route path="/coordonator" component={Coordonator}/>
        <Route path="/convertor" component={ConvertXMLtoREACT}/>
        <Route path="/success" component={Success}/>
        <Route path="/login" component={Login}/>
        <Route path="/signup"component={Signup}/>
        <Route path="/logout"component={Logout}/>



    </Switch>
)

export default Main;
