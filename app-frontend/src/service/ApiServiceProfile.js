import axios from 'axios';

const PROFILE_API_BASE_URL = 'http://localhost:8000/api/profiles';
const GET_PROFILE_ID_API_BASE_URL = 'http://localhost:8000/api/ProfileId';
class ApiServiceProfile {

    updateUserProfile(profile) {
        return axios.put(PROFILE_API_BASE_URL + '/' + profile.id, profile);
    }
    createUserProfile(profile) {
        return axios.post(""+PROFILE_API_BASE_URL,profile);
    }

    getProfileByUserId(user_id){
        return axios.get(GET_PROFILE_ID_API_BASE_URL+'/'+user_id);
    }

}

export default new ApiServiceProfile();


// editPatient(patient) {
// //     return axios.put(PATIENT_API_BASE_URL + '/' + patient.id, patient);
// // }
