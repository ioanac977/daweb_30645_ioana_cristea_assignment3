import React from "react";

export const translations = {
    'en': {
        'text1': 'Choose your language:',
        'text2': 'Language',
        'acasa/home': 'Home',
        'noutati/news': 'News',
        'despre/about': 'About',
        'profil/profile': 'Profile',
        'coordonator/coordinator': 'Coordinator',
        'contact/contact': 'Contact',
        'bunVenit/welcome': 'Welcome ! ',
        'descriere/describe': 'How would it be if we organized our life so well that we do not have to calculate how long it ' +
            'takes us to go from one place to another, how long we have to stay, and do not even bother to remember where we were a week ago?',
        'citesteMaiMulte/readMore': 'Read More',
        'titluStire/titleNews': 'Google records your location even when you tell it not to',
        'numeContact/nameContact': 'Contact me',
        'descriereCoordonator/descriptionCoordinator':'Computer Networks and Internet Technologies | Distributed Systems | High Performance Computing | Advanced Architectures for Parallel Computer',
         'functieCoordonator/functionCoordinator' : 'Senior Lecturer Engineer',
         'obiectiv/objectiv': 'Objectiv'



    },
    'ro': {
        'text1': 'Dil Seçiniz:',
        'text2': 'Türkçe',
        'acasa/home': 'Acasa',
        'noutati/news': 'Noutati',
        'despre/about': 'Despre',
        'profil/profile': 'Profil',
        'coordonator/coordinator': 'Coordonator',
        'contact/contact': 'Contact',
        'bunVenit/welcome': 'Bine ați venit !',
        'descriere/describe': 'Cum ar fi dacă ne-am organiza viața atât de bine încât nu trebuie să calculăm cât timp\n' +
            '              ne ia să mergem dintr-un loc în altul, cât trebuie să rămânem și nici să incercăm să ne mai amintim\n' +
            '              unde am fost acum o săptămână?',
        'citesteMaiMulte/readMore': 'Citește mai multe',
        'titluStire/titleNews': 'Google îți înregistrează locația chiar și atunci când i-ai spus să nu o facă',
        'numeContact/nameContact': 'Contactează-mă',
        'descriereCoordonator/descriptionCoordinator':'Rețele de calculatoare și tehnologii de internet | Sisteme distribuite | Calcul de înaltă performanță | Arhitecturi avansate pentru sisteme paralele',
        'functieCoordonator/functionCoordinator' : 'Inginer conferențiar principal',
        'obiectiv/objectiv': 'Obiectivul'
    }
}
