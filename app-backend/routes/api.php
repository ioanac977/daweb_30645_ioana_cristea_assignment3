<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::get('profile', 'UserController@getAuthenticatedUser');
Route::get('UserId/{email}', 'UserController@getUserId');

Route::get('comments','CommentsController@index');
Route::get('comments/{id}','CommentsController@show');
Route::post('comments','CommentsController@store');
Route::delete('comments/{id}','CommentsController@delete');

Route::get('profiles','ProfilesController@index');
Route::get('profiles/{id}','ProfilesController@show');
Route::get('ProfileId/{user_id}','ProfilesController@getProfileId');

Route::post('profiles','ProfilesController@store');
Route::put('profiles/{id}','ProfilesController@update');
Route::delete('profiles/{id}','ProfilesController@delete');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
