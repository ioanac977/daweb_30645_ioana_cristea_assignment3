<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    protected $fillable = ['name',"email","password","interests","user_id"];


    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
