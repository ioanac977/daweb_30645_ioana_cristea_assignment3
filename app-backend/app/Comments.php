<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = ['description',"user_id"];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
