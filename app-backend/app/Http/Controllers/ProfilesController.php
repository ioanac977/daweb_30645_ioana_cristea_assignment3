<?php

namespace App\Http\Controllers;

use App\Profiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Image;

class ProfilesController extends Controller
{
    public function index(){
        return Profiles::all();
    }
    public function show($id){
        return Profiles::find($id);
    }
    public function store(Request $request){

     return  Profiles::create($request->all());

    }
    public function update(Request $request,$id){
        $profiles = Profiles::findOrFail($id);
        $profiles->update($request->all());
        return $profiles;
    }
    public function delete(Request $request,$id){
        $profiles = Profiles::findOrFail($id);
        $profiles->delete();
        return 204;
    }
    public function getProfileId($user_id)
    {

        $results = DB::select('select id from profiles where user_id = :user_id', ['user_id' => $user_id]);
        return $results;
    }
}
