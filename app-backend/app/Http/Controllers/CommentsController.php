<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comments;

class CommentsController extends Controller
{
    public function index(){
        return Comments::all();
    }
    public function show($id){
        return Comments::find($id);
    }
    public function store(Request $request){
        return  Comments::create($request->all());
    }
    public function update(Request $request,$id){
        $comments = Comments::findOrFail($id);
        $comments->update($request->all());
            return $comments;
    }
    public function delete(Request $request,$id){
        $comments = Comments::findOrFail($id);
        $comments->delete();
        return 204;
    }

}
