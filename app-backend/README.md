# Configurations & Instalation 

For executing the project the folowing are required : 

PHP version >=7.3.9 

MySQL (version > 5)

Apache/Nginx Server

In php.ini file from your PHP installation directory , activate the sql/mysql extension .

cd into project directory 

open a terminal and execute : php artisan serve 

This will start the laravel project on port 8000.

Open http://localhost/phpmyadmin and you can see your databases content .

The project's operations are easier to see using the above link 


