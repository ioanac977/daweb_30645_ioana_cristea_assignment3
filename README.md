# DESCRIPTION 
Starting from the web application developed in assignment 2, implement the following new functionalities.
# REQUIREMENTS
1. User registration / login (email & password). To preserve the status of the user after login will use sessions
2. For logged in users:
2.1 User profile page that contains the following information that can be changed: Name, Email, Password, Areas of interest (select multiple values ​​from the list), Profile image (upload)
2.2  Adding comments in About section
# REMARKS 
- use PHP technologies to implement the new functionalities (use REST services). The use of any existing Framework that makes advanced libraries available is accepted for these technologies (Laravel).
- Data persistence is done using databases (MySQL).
# CONFIGURATIONS 
- install NodeJs 
- install PHP
- install MySQL
- install Apache/Nginx Server
- install WebStorm
- install PhpStorm
# RUN
- download repository
## FRONTEND
- open frontend project using WebStorm
- go to src directory 
- run on terminal "npm install"
- run on terminal "npm start"
## BACKEND 
- open frontend project using PhpStorm
- run on terminal "php artisan serve"
# NAVIGATION 
- the frontend application will start on http://localhost:3000/
- the backend application will start on  http://localhost:8000/

